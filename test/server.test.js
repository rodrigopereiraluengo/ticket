const request = require('supertest');

const app = require('../src/app');
const httpStatus = require('../src/enums/http-status');

test('It should running', (done) => {
  request(app).get('/').expect(httpStatus.OK, done);
});
