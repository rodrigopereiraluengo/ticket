const request = require('supertest');

const app = require('../src/app');
const httpStatus = require('../src/enums/http-status');

const agent = request.agent(app);

test('It should validate required login user', (done) => {
  agent
    .post('/auth')
    .expect(httpStatus.UNPROCESSABLE_ENTITY, done);
});

test('It should validate type login user', (done) => {
  agent
    .post('/auth')
    .send({ email: 'notanemail', password: 'asdasd' })
    .expect(httpStatus.UNPROCESSABLE_ENTITY, done);
});

test('It should validate login user', (done) => {
  agent
    .post('/auth')
    .send({ email: 'email@email.com', password: '123456' })
    .expect(httpStatus.UNAUTHORIZED, done);
});

test('It should reject login user with wrong password', (done) => {
  agent
    .post('/auth')
    .send({ email: 'rpl@outlook.com', password: 'secret1' })
    .expect(httpStatus.UNAUTHORIZED, done);
});

describe('Logged user test', () => {
  let token;
  test('It should login user', (done) => {
    agent
      .post('/auth')
      .send({ email: 'rpl@outlook.com', password: 'secret' })
      .expect(httpStatus.OK)
      .then(response => {
        if (!('token' in response.body)) throw new Error('token not found');
        if (!('user_id' in response.body)) throw new Error('user_id not found');
        if (!('email' in response.body)) throw new Error('email not found');
        token = response.body.token;
        done();
      })
      .catch(err => done(err));
  });

  test('It should getting a login user', (done) => {
    agent
      .get('/auth')
      .set('Authorization', token)
      .expect(httpStatus.OK)
      .then(response => {
        if (!('id' in response.body)) throw new Error('id not found');
        if (!('subscription_id' in response.body)) throw new Error('subscription_id not found');
        if (!('status' in response.body)) throw new Error('status not found');
        if (!('email' in response.body)) throw new Error('email not found');
        if (!('authorization' in response.headers)) throw new Error('authorization header not found');
        token = response.headers.authorization;
        done();
      });
  });

  test('It should logout a logged user', (done) => {
    agent
      .delete('/auth')
      .set('Authorization', token)
      .expect(httpStatus.NO_CONTENT, done);
  });

  test('It should not get unlogged user', (done) => {
    agent
      .get('/auth')
      .set('Authorization', token)
      .expect(httpStatus.UNAUTHORIZED, done);
  });
});
