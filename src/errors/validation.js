class ValidationError extends Error {
  constructor(errors) {
    super();
    this.errors = errors;
    this.message = 'Validation errors';
  }
}

module.exports = ValidationError;
