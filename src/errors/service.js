class ServiceError extends Error {
  constructor(message) {
    super();
    this.message = message;
  }
}

module.exports = ServiceError;
