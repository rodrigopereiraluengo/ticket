class UnauthorizedError extends Error {
  constructor(message = 'Unauthorized') {
    super();
    this.message = message;
  }
}

module.exports = UnauthorizedError;
