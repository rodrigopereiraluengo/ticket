const httpStatus = require('../enums/http-status');

module.exports = ({ services }) => ({

  post: async (req, res) => {
    const auth = await services.auth.login(req.body);
    res.json(auth);
  },

  get: async (req, res) => {
    res.json(req.user);
  },

  delete: async (req, res) => {
    await services.auth.logout(req.user);
    res.status(httpStatus.NO_CONTENT).json();
  },

});
