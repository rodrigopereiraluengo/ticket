const ServiceError = require('../errors/service');

module.exports = ({ models }) => ({

  index: async (req, res) => {
    res.json(await models.user().select().first());
  }

});
