const asyncfn = require('../helpers/asyncfn');

module.exports = app => {
  const authMiddleware = asyncfn(app.middlewares.auth);
  const refreshTokenMiddleware = asyncfn(app.middlewares.refreshtoken);

  const homeIndex = asyncfn(app.routes.home.index);

  const authPost = asyncfn(app.routes.auth.post);
  const authGet = asyncfn(app.routes.auth.get);
  const authDelete = asyncfn(app.routes.auth.delete);

  // Home
  app.get('/', homeIndex);

  // Authentication
  app.post('/auth', authPost);
  app.get('/auth', authMiddleware, refreshTokenMiddleware, authGet);
  app.delete('/auth', authMiddleware, authDelete);
};
