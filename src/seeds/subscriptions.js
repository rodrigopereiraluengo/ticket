exports.seed = knex => knex('subscriptions').del()
  .then(async () => {
    const plan = await knex('plans').first();

    return knex('subscriptions').insert([
      { plan_id: plan.id, status: true }
    ]);
  });
