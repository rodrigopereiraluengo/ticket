const bcrypt = require('bcrypt');

exports.seed = knex => knex('users').del()
  .then(async () => {
    const subscription = await knex('subscriptions').first();

    const salt = bcrypt.genSaltSync(parseInt(process.env.BCRYPT_SALT));

    const password = bcrypt.hashSync('secret', salt);

    return knex('users').insert([
      {
        subscription_id: subscription.id,
        status: true,
        email: 'rpl@outlook.com',
        password
      }
    ]);
  });
