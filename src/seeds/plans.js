exports.seed = knex => knex('plans').del()
  .then(() => knex('plans').insert([
    { name: 'Plan 1', status: true },
    { name: 'Plan 2', status: true },
    { name: 'Plan 3', status: true }
  ]));
