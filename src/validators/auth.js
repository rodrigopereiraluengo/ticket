const Validator = require('validatorjs');

module.exports = () => ({

  login(data) {
    const validation = new Validator(data, {
      email: 'required|email',
      password: 'required'
    });

    return validation;
  }

});
