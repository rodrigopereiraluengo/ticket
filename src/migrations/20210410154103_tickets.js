exports.up = knex => knex.schema.createTable('tickets', table => {
  table.increments();
  table.bigInteger('subscription_id').notNullable();
  table.string('status').default('OPENED', 11); /* OPENED, IN PROGRESS, FINISHED, CANCELED */
  table.string('priority').notNullable().default('DEFAULT', 7);/* LOW, DEFAULT, HIGH */
  table.string('email').notNullable();
  table.string('subject').notNullable();
  table.timestamps();
  table
    .foreign('subscription_id')
    .references('subscriptions.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('tickets');
