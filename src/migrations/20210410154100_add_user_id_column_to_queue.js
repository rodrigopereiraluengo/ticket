exports.up = knex => knex.schema.alterTable('queue', table => {
  table.bigInteger('user_id');
  table
    .foreign('user_id')
    .references('users.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('queue');
