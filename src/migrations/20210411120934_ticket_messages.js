exports.up = knex => knex.schema.createTable('ticket_messages', table => {
  table.increments();
  table.bigInteger('ticket_id').notNullable();
  table.bigInteger('user_id');
  table.boolean('visible').notNullable().default(false);
  table.string('message', 4000).notNullable();
  table.timestamps();
  table
    .foreign('ticket_id')
    .references('tickets.id')
    .onDelete('cascade')
    .onUpdate('cascade');
  table
    .foreign('user_id')
    .references('users.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('ticket_messages');
