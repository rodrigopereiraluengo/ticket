exports.up = knex => knex.schema.createTable('plans', table => {
  table.increments();
  table.boolean('status');
  table.string('name', 45).notNullable();
  table.timestamps();

  table.unique('name');
});

exports.down = knex => knex.schema.dropTable('plans');
