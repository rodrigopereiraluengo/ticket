exports.up = knex => knex.schema.createTable('queue', table => {
  table.increments();
  table.bigInteger('subscription_id').notNullable();
  table.boolean('status');
  table.string('name').notNullable();
  table.timestamps();
  table.unique('subscription_id', 'name');

  table
    .foreign('subscription_id')
    .references('subscriptions.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('queue');
