exports.up = knex => knex.schema.createTable('users', table => {
  table.increments();
  table.bigInteger('subscription_id').notNullable();
  table.bigInteger('department_id').notNullable();
  table.boolean('status');
  table.string('email').notNullable();
  table.string('password').notNullable();
  table.string('token');
  table.dateTime('token_expire_at');
  table.string('reset_password');
  table.dateTime('reset_password_expire_at');

  table.timestamps();
  table.unique('email');

  table
    .foreign('subscription_id')
    .references('subscriptions.id')
    .onDelete('cascade')
    .onUpdate('cascade');
  table
    .foreign('department_id')
    .references('departments.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('users');
