exports.up = knex => knex.schema.createTable('ticket_files', table => {
  table.increments();
  table.bigInteger('ticket_message_id').notNullable();
  table.string('name').notNullable();
  table.string('original_name').notNullable();
  table.integer('size').notNullable();
  table.integer('size').notNullable();
  table.timestamps();
  table
    .foreign('ticket_message_id')
    .references('ticket_messages.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('ticket_files');
