exports.up = knex => knex.schema.createTable('subscriptions', table => {
  table.increments();
  table.bigInteger('plan_id').notNullable();
  table.boolean('status');

  table.timestamps();

  table
    .foreign('plan_id')
    .references('plans.id')
    .onDelete('cascade')
    .onUpdate('cascade');
});

exports.down = knex => knex.schema.dropTable('subscriptions');
