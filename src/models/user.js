module.exports = ({ db }) => () => {
  const model = db('users');

  /*model.defaultSelect = async (data) => {
    return await model.select(['status', 'id', 'email']).paginate({ perPage: 10, currentPage: 2 });
    /*return Promise.all(users.map((user) => new Promise((resolve, reject) => {
      app.models.imovel().where('owner_id', user.id).select().then((imoveis) => {
        resolve({ ...user, imoveis });
      })
        .catch((e) => reject(e));
    })));
  };*/

  model.findByEmail = (email) => model.where('email', email).first();

  model.updateToken = (id, token, token_expire_at) => model.where('id', id).update({ token, token_expire_at });

  return model;
};
