const jwt = require('jsonwebtoken');
const { DateTime } = require('luxon');

module.exports = ({ models }) => async (req, res, next) => {
  if (req.user) {
    const authentication = req.header('Authorization').replace(/^bearer/ig, '').trim();
    const decoded = jwt.verify(authentication, process.env.JWT_SECRET);
    const token = jwt.sign({ id: decoded.id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_TIMEOUT
    });

    res.set('Authorization', token);
    const token_expire_at = DateTime.now().plus({ seconds: process.env.JWT_TIMEOUT });
    await models.user().updateToken(decoded.id, token, token_expire_at);
  }
  next();
};
