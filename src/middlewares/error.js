const httpStatus = require('../enums/http-status');
const UnauthorizedError = require('../errors/unauthorized');
const NotFoundError = require('../errors/not-found');
const ServiceError = require('../errors/service');
const ValidationError = require('../errors/validation');

module.exports = async (err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    return res
      .status(httpStatus.UNAUTHORIZED)
      .json({ message: err.message });
  }

  if (err instanceof NotFoundError) {
    return res
      .status(httpStatus.NOT_FOUND)
      .json({ message: err.message });
  }

  if (err instanceof ValidationError) {
    return res
      .status(httpStatus.UNPROCESSABLE_ENTITY)
      .json({
        ...err.errors.errors
      });
  }

  if (err instanceof ServiceError) {
    return res
      .status(httpStatus.BAD_REQUEST)
      .json({ message: err.message });
  }

  return next(err);
};
