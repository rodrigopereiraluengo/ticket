const jwt = require('jsonwebtoken');
const UnauthorizedError = require('../errors/unauthorized');

module.exports = ({ models }) => async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace(/^bearer/ig, '').trim();
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const user = await models.user().where('id', decoded.id).select(['id', 'status', 'email', 'subscription_id', 'token']).first();
    if (!user) {
      throw new UnauthorizedError();
    }

    if (user.token !== token || !user.token) {
      throw new UnauthorizedError();
    }

    req.user = user;
  } catch (err) {
    throw new UnauthorizedError();
  }
  next();
};
