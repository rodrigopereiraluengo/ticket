const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { DateTime } = require('luxon');

const UnauthorizedError = require('../errors/unauthorized');
const ValidationError = require('../errors/validation');

module.exports = ({ validators, models }) => ({

  login: async (data) => {
    const validator = validators.auth.login(data);
    if (validator.fails()) {
      throw new ValidationError(validator.errors);
    }

    const user = await models.user().findByEmail(data.email);
    if (typeof user === 'undefined') {
      throw new UnauthorizedError();
    }

    if (!bcrypt.compareSync(data.password, user.password)) {
      throw new UnauthorizedError();
    }

    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_TIMEOUT
    });
    const token_expire_at = DateTime.now().plus({ seconds: process.env.JWT_TIMEOUT });
    await models.user().updateToken(user.id, token, token_expire_at);

    return {
      token,
      user_id: user.id,
      email: user.email
    };
  },

  logout: async user => {
    await models.user().where('id', user.id).update({ token: null, token_expire_at: null });
  }

});
