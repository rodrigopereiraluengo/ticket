require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const consign = require('consign');
const knex = require('knex');
const { attachPaginate } = require('knex-paginate');
const knexfile = require('../knexfile');
const error = require('./middlewares/error');

const db = knex(knexfile.test);
attachPaginate();

const app = express();

app.db = db;

app.use(bodyParser.json());

consign({ cwd: 'src', verbose: false })
  .include('./models')
  .then('./validators')
  .then('./services')
  .then('./routes')
  .then('./middlewares/auth.js')
  .then('./middlewares/refreshtoken.js')
  .then('./config/routes.js')
  .into(app);

app.use(error);

module.exports = app;
